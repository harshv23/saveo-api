package com.saveo.hiring.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = { "com.saveo.hiring" })
@EnableJpaRepositories(basePackages = { "com.saveo.hiring.dao" })
@EntityScan(basePackages = { "com.saveo.hiring.dao" })
@EnableAutoConfiguration
public class SaveoWebApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(SaveoWebApplication.class, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(SaveoWebApplication.class);
  }

}
