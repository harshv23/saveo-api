package com.saveo.hiring.web.app.controller.validator;

import java.util.List;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.model.OrderProduct;

public class SaveoValidator {

  public static void validateSearchMedicine(String name) throws SaveoBusinessException {

    if ((name == null) || name.isEmpty()) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "name is mandatory. ");
    }

  }

  public static void validateGetMedicineDetails(Long productId) throws SaveoBusinessException {

    if (productId == null) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "product id is mandatory. ");
    }
  }

  public static void validatePlaceOrder(List<OrderProduct> orderProductList)
      throws SaveoBusinessException {
    if ((orderProductList == null) || orderProductList.isEmpty()) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "list of products is mandatory. ");
    }

    for (OrderProduct orderProduct : orderProductList) {
      validateOrderProduct(orderProduct);
    }

  }

  private static void validateOrderProduct(OrderProduct orderProduct)
      throws SaveoBusinessException {
    if ((orderProduct.getC_name() == null) || orderProduct.getC_name().isEmpty()) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "Product name is mandatory. ");
    }

    if ((orderProduct.getC_unique_id() == null)) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "Product id is mandatory.");
    }

    if (orderProduct.getQuantity() == null) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "Quantity is mandatory. ");
    }

    if (orderProduct.getQuantity() < 0) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.VALIDATION_ERROR.getErrorCode(),
          "Quantity cannot be negative. ");
    }
  }



}
