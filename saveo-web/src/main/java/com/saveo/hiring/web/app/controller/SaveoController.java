package com.saveo.hiring.web.app.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.interfaces.ISaveoManagerService;
import com.saveo.hiring.common.model.GetProductResponse;
import com.saveo.hiring.common.model.OrderProduct;
import com.saveo.hiring.common.model.PlaceOrderResponse;
import com.saveo.hiring.common.model.SearchMedicineResponse;
import com.saveo.hiring.common.model.UploadCsvResponse;
import com.saveo.hiring.service.util.FileUtils;
import com.saveo.hiring.web.app.controller.validator.SaveoValidator;


@RestController
@RequestMapping("saveo/api")
public class SaveoController {

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private ISaveoManagerService saveoManagerService;

  @GetMapping("/health-check")
  public @ResponseBody Boolean healthCheck() {
    logger.info("Request received.");
    return true;
  }

  @PostMapping("/uploadCSV")
  public @ResponseBody UploadCsvResponse uploadCSV(@RequestParam("csvFile") MultipartFile csvFile)
      throws SaveoBusinessException {
    logger.info("Request received with csvFile: " + csvFile);

    try {
      if (!FileUtils.hasCSVFormat(csvFile)) {
        throw new SaveoBusinessException(SaveoErrorCodeEnum.INVALID_FILE_FORMAT.getErrorCode(),
            SaveoErrorCodeEnum.INVALID_FILE_FORMAT.getErrorMessage());
      }
      // File is a valid csv file
      return saveoManagerService.uploadCsv(csvFile.getInputStream());
    } catch (SaveoBusinessException e) {
      logger.error("Error occurred while uploading csv file", e);
      UploadCsvResponse uploadCsvResponse = new UploadCsvResponse();
      uploadCsvResponse.setIsSuccess(false);
      uploadCsvResponse.setErrorCode(e.getErrorCode());
      uploadCsvResponse.setErrorMessage(e.getErrorMessage());
      return uploadCsvResponse;
    } catch (Exception e) {
      logger.error("Error occurred while uploading csv file", e);
      UploadCsvResponse uploadCsvResponse = new UploadCsvResponse();
      uploadCsvResponse.setIsSuccess(false);
      uploadCsvResponse.setErrorCode(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorCode());
      uploadCsvResponse.setErrorMessage(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorMessage());
      return uploadCsvResponse;
    }

  }

  @GetMapping("/searchMedicine")
  public @ResponseBody SearchMedicineResponse searchMedicine(@RequestParam("name") String name)
      throws SaveoBusinessException {
    logger.info("Request received with name: " + name);

    try {
      SaveoValidator.validateSearchMedicine(name);
      return saveoManagerService.getProductListWithName(name);
    } catch (SaveoBusinessException e) {
      logger.error("Error occurred while searching with name : " + name, e);
      SearchMedicineResponse searchMedicineResponse = new SearchMedicineResponse();
      searchMedicineResponse.setIsSuccess(false);
      searchMedicineResponse.setErrorCode(e.getErrorCode());
      searchMedicineResponse.setErrorMessage(e.getErrorMessage());
      return searchMedicineResponse;
    } catch (Exception e) {
      logger.error("Error occurred while searching with name : " + name, e);
      SearchMedicineResponse searchMedicineResponse = new SearchMedicineResponse();
      searchMedicineResponse.setIsSuccess(false);
      searchMedicineResponse.setErrorCode(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorCode());
      searchMedicineResponse.setErrorMessage(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorMessage());
      return searchMedicineResponse;
    }

  }

  @GetMapping("/getMedicineDetails")
  public @ResponseBody GetProductResponse getMedicineDetails(
      @RequestParam("c_unique_id") Long productId) throws SaveoBusinessException {
    logger.info("Request received with productId: " + productId);

    try {
      SaveoValidator.validateGetMedicineDetails(productId);
      return saveoManagerService.getProductById(productId);
    } catch (SaveoBusinessException e) {
      logger.error("Error occurred while getting product with id: " + productId, e);
      GetProductResponse getProductResponse = new GetProductResponse();
      getProductResponse.setIsSuccess(false);
      getProductResponse.setErrorCode(e.getErrorCode());
      getProductResponse.setErrorMessage(e.getErrorMessage());
      return getProductResponse;
    } catch (Exception e) {
      logger.error("Error occurred while getting product with id: " + productId, e);
      GetProductResponse getProductResponse = new GetProductResponse();
      getProductResponse.setIsSuccess(false);
      getProductResponse.setErrorCode(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorCode());
      getProductResponse.setErrorMessage(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorMessage());
      return getProductResponse;
    }

  }


  @PostMapping("/placeorder")
  public @ResponseBody PlaceOrderResponse placeorder(
      @RequestBody List<OrderProduct> orderProductList) throws SaveoBusinessException {
    logger.info("Request received with orderProductList: " + orderProductList);

    try {
      // File is a valid csv file
      SaveoValidator.validatePlaceOrder(orderProductList);
      return saveoManagerService.placeOrder(orderProductList);
    } catch (SaveoBusinessException e) {
      logger.error("Error occurred while uploading csv file", e);
      PlaceOrderResponse placeOrderResponse = new PlaceOrderResponse();
      placeOrderResponse.setIsSuccess(false);
      placeOrderResponse.setErrorCode(e.getErrorCode());
      placeOrderResponse.setErrorMessage(e.getErrorMessage());
      return placeOrderResponse;
    } catch (Exception e) {
      logger.error("Error occurred while uploading csv file", e);
      PlaceOrderResponse placeOrderResponse = new PlaceOrderResponse();
      placeOrderResponse.setIsSuccess(false);
      placeOrderResponse.setErrorCode(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorCode());
      placeOrderResponse.setErrorMessage(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorMessage());
      return placeOrderResponse;
    }

  }

}
