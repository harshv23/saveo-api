package com.saveo.hiring.common.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class ProductResponse implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -9158333845828257404L;

  private Long id;
  private String name;
  private String code;
  private String hsnCode;
  private Long manufacturerId;
  private String manufacturerName;
  private Long packagingTypeId;
  private String packagingTypeName;
  private Boolean isActive;
  private Date createdOn;
  private Timestamp updatedOn;
  private String createdBy;
  private String updatedBy;

  private List<ProductBatchLite> productBatchList;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getHsnCode() {
    return hsnCode;
  }

  public void setHsnCode(String hsnCode) {
    this.hsnCode = hsnCode;
  }

  public Long getManufacturerId() {
    return manufacturerId;
  }

  public void setManufacturerId(Long manufacturerId) {
    this.manufacturerId = manufacturerId;
  }

  public String getManufacturerName() {
    return manufacturerName;
  }

  public void setManufacturerName(String manufacturerName) {
    this.manufacturerName = manufacturerName;
  }

  public Long getPackagingTypeId() {
    return packagingTypeId;
  }

  public void setPackagingTypeId(Long packagingTypeId) {
    this.packagingTypeId = packagingTypeId;
  }

  public String getPackagingTypeName() {
    return packagingTypeName;
  }

  public void setPackagingTypeName(String packagingTypeName) {
    this.packagingTypeName = packagingTypeName;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Timestamp getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(Timestamp updatedOn) {
    this.updatedOn = updatedOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public List<ProductBatchLite> getProductBatchList() {
    return productBatchList;
  }

  public void setProductBatchList(List<ProductBatchLite> productBatchList) {
    this.productBatchList = productBatchList;
  }

  @Override
  public String toString() {
    return "ProductResponse [id=" + id + ", name=" + name + ", code=" + code + ", hsnCode="
        + hsnCode + ", manufacturerId=" + manufacturerId + ", manufacturerName=" + manufacturerName
        + ", packagingTypeId=" + packagingTypeId + ", packagingTypeName=" + packagingTypeName
        + ", isActive=" + isActive + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn
        + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + ", productBatchList="
        + productBatchList + "]";
  }

}
