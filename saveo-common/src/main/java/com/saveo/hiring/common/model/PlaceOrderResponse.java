package com.saveo.hiring.common.model;

import java.io.Serializable;

public class PlaceOrderResponse implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 9032691066761628313L;

  private Boolean isSuccess;
  private String errorCode;
  private String errorMessage;

  private String orderNo;

  public Boolean getIsSuccess() {
    return isSuccess;
  }

  public void setIsSuccess(Boolean isSuccess) {
    this.isSuccess = isSuccess;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  @Override
  public String toString() {
    return "PlaceOrderResponse [isSuccess=" + isSuccess + ", errorCode=" + errorCode
        + ", errorMessage=" + errorMessage + ", orderNo=" + orderNo + "]";
  }

}
