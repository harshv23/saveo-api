package com.saveo.hiring.common.model;

import java.io.Serializable;
import java.util.Date;

public class ProductBatchLite implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 6302566594566604489L;

  private Long id;
  private String batchNo;
  private Date expiryDate;
  private Long productId;
  private Double mrp;
  private Integer initialQuantity;
  private Integer availableQuantity;
  private Boolean isActive;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Double getMrp() {
    return mrp;
  }

  public void setMrp(Double mrp) {
    this.mrp = mrp;
  }

  public Integer getInitialQuantity() {
    return initialQuantity;
  }

  public void setInitialQuantity(Integer initialQuantity) {
    this.initialQuantity = initialQuantity;
  }

  public Integer getAvailableQuantity() {
    return availableQuantity;
  }

  public void setAvailableQuantity(Integer availableQuantity) {
    this.availableQuantity = availableQuantity;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  @Override
  public String toString() {
    return "ProductBatchLite [id=" + id + ", batchNo=" + batchNo + ", expiryDate=" + expiryDate
        + ", productId=" + productId + ", mrp=" + mrp + ", initialQuantity=" + initialQuantity
        + ", availableQuantity=" + availableQuantity + ", isActive=" + isActive + "]";
  }

}
