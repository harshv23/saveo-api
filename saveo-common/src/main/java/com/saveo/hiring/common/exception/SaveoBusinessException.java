package com.saveo.hiring.common.exception;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class SaveoBusinessException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = -475020808119618445L;


  private String errorCode;
  private String errorMessage;

  public SaveoBusinessException(String errorCode, String errorMessage) {
    super();
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String toString() {
    return "SaveoBusinessException [errorCode=" + errorCode + ", errorMessage=" + errorMessage
        + "]";
  }


}
