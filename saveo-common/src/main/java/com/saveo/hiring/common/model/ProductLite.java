package com.saveo.hiring.common.model;

public class ProductLite {

  private Long productId;
  private String productName;

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  @Override
  public String toString() {
    return "ProductLite [productId=" + productId + ", productName=" + productName + "]";
  }


}
