package com.saveo.hiring.common.model;

import java.io.Serializable;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class UploadCsvResponse implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 4166305618359014182L;

  private Boolean isSuccess;
  private String errorCode;
  private String errorMessage;

  public Boolean getIsSuccess() {
    return isSuccess;
  }

  public void setIsSuccess(Boolean isSuccess) {
    this.isSuccess = isSuccess;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String toString() {
    return "UploadCsvResponse [isSuccess=" + isSuccess + ", errorCode=" + errorCode
        + ", errorMessage=" + errorMessage + "]";
  }

}
