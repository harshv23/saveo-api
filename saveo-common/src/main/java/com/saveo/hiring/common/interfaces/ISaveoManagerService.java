package com.saveo.hiring.common.interfaces;

import java.io.InputStream;
import java.util.List;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.model.GetProductResponse;
import com.saveo.hiring.common.model.OrderProduct;
import com.saveo.hiring.common.model.PlaceOrderResponse;
import com.saveo.hiring.common.model.SearchMedicineResponse;
import com.saveo.hiring.common.model.UploadCsvResponse;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface ISaveoManagerService {

  UploadCsvResponse uploadCsv(InputStream csvInputStream) throws SaveoBusinessException;

  SearchMedicineResponse getProductListWithName(String name) throws SaveoBusinessException;

  GetProductResponse getProductById(Long productId) throws SaveoBusinessException;

  PlaceOrderResponse placeOrder(List<OrderProduct> orderProducts) throws SaveoBusinessException;

}
