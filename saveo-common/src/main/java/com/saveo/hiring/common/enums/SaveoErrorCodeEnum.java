package com.saveo.hiring.common.enums;

/**
 *
 * @author Harshwardhan Singh Muktawat
 * @createdOn 2nd June 2021
 *
 */
public enum SaveoErrorCodeEnum {

  VALIDATION_ERROR("VALIDATION_ERROR", "Request Validation failed. "),
  DATABASE_ERROR("DATABASE_ERROR", "SQL error occurred while interacting with db. "),
  INTERNAL_ERROR("INTERNAL_ERROR", "Internal Error occurred. Please contact Saveo support team."),
  DB_FETCH_ERROR("DB_FETCH_ERROR", "Error occurred while fetching from db. "),
  INVALID_FILE_FORMAT("INVALID_FILE", "Invalid file uploaded. Kindly upload a csv file. "),
  INVALID_VALUES_CSV("INVALID_VALUES_CSV", "Csv file contains invalid values. "),
  NO_PRODUCTS_FOUND("NO_PRODUCTS_FOUND", "No products found. "),
  MANUFACTURER_NOT_FOUND("MANUFACTURER_NOT_FOUND", "Manufacturer not found for product. "),
  PACKAGING_TYPE_NOT_FOUND("PACKAGING_TYPE_NOT_FOUND", "Packagin type not found for product. "),
  INVALID_PRODUCT_ID("INVALID_PRODUCT_ID", "Invalid product id.");

  private String errorCode;
  private String errorMessage;

  private SaveoErrorCodeEnum(String errorCode, String errorMessage) {
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }


}
