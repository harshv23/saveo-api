package com.saveo.hiring.common.model;

import java.io.Serializable;

public class OrderProduct implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -6124881859708440443L;
  private Long c_unique_id;
  private Integer quantity;
  private String c_name;

  public Long getC_unique_id() {
    return c_unique_id;
  }

  public void setC_unique_id(Long c_unique_id) {
    this.c_unique_id = c_unique_id;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public String getC_name() {
    return c_name;
  }

  public void setC_name(String c_name) {
    this.c_name = c_name;
  }

  @Override
  public String toString() {
    return "PlaceOrderRequest [c_unique_id=" + c_unique_id + ", quantity=" + quantity + ", c_name="
        + c_name + "]";
  }

}
