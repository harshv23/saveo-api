package com.saveo.hiring.common.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class SearchMedicineResponse implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 7573572294019050695L;

  private Boolean isSuccess;
  private String errorCode;
  private String errorMessage;

  private List<ProductLite> productLiteList;

  public Boolean getIsSuccess() {
    return isSuccess;
  }

  public void setIsSuccess(Boolean isSuccess) {
    this.isSuccess = isSuccess;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public List<ProductLite> getProductLiteList() {
    return productLiteList;
  }

  public void setProductLiteList(List<ProductLite> productLiteList) {
    this.productLiteList = productLiteList;
  }

  @Override
  public String toString() {
    return "SearchMedicineResponse [isSuccess=" + isSuccess + ", errorCode=" + errorCode
        + ", errorMessage=" + errorMessage + ", productLiteList=" + productLiteList + "]";
  }

}
