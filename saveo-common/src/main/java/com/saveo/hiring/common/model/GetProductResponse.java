package com.saveo.hiring.common.model;

import java.io.Serializable;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class GetProductResponse implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 5141336890658247545L;

  private Boolean isSuccess;
  private String errorCode;
  private String errorMessage;
  private ProductResponse productResponse;

  public Boolean getIsSuccess() {
    return isSuccess;
  }

  public void setIsSuccess(Boolean isSuccess) {
    this.isSuccess = isSuccess;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public ProductResponse getProductResponse() {
    return productResponse;
  }

  public void setProductResponse(ProductResponse productResponse) {
    this.productResponse = productResponse;
  }

  @Override
  public String toString() {
    return "GetProductResponse [isSuccess=" + isSuccess + ", errorCode=" + errorCode
        + ", errorMessage=" + errorMessage + ", productResponse=" + productResponse + "]";
  }

}
