package com.saveo.hiring.service.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.saveo.hiring.dao.model.PackagingTypeEntity;
import com.saveo.hiring.service.model.PackagingType;

public class PackagingTypeMapper {

  public static Map<String, PackagingType> mapEntityListToMap(
      List<PackagingTypeEntity> packagingTypeEntityList) {

    Map<String, PackagingType> map = new HashMap<>();

    packagingTypeEntityList.forEach(r -> map.put(r.getName(), mapEntityToPackagingType(r)));

    return map;
  }

  public static PackagingType mapEntityToPackagingType(PackagingTypeEntity packagingTypeEntity) {
    PackagingType packagingType = new PackagingType();
    packagingType.setCreatedBy(packagingTypeEntity.getCreatedBy());
    packagingType.setCreatedOn(packagingTypeEntity.getCreatedOn());
    packagingType.setId(packagingTypeEntity.getId());
    packagingType.setName(packagingTypeEntity.getName());
    return packagingType;
  }

  public static Map<Long, PackagingType> mapEntityListToIdMap(
      List<PackagingTypeEntity> packagingTypeEntityList) {


    Map<Long, PackagingType> map = new HashMap<>();

    packagingTypeEntityList.forEach(r -> map.put(r.getId(), mapEntityToPackagingType(r)));

    return map;

  }

}
