package com.saveo.hiring.service.mapper;

import java.util.ArrayList;
import java.util.List;

import com.saveo.hiring.common.model.ProductBatchLite;
import com.saveo.hiring.dao.model.ProductBatchEntity;
import com.saveo.hiring.service.model.ProductBatch;

public class ProductBatchMapper {

  public static ProductBatchEntity mapProductBatchToEntity(ProductBatch productBatch) {
    ProductBatchEntity productBatchEntity = new ProductBatchEntity();
    productBatchEntity.setAvailableQuantity(productBatch.getAvailableQuantity());
    productBatchEntity.setBatchNo(productBatch.getBatchNo());
    productBatchEntity.setCreatedBy(productBatch.getCreatedBy());
    productBatchEntity.setCreatedOn(productBatch.getCreatedOn());
    productBatchEntity.setExpiryDate(productBatch.getExpiryDate());
    productBatchEntity.setId(productBatch.getId());
    productBatchEntity.setInitialQuantity(productBatch.getInitialQuantity());
    productBatchEntity.setIsActive(productBatch.getIsActive());
    productBatchEntity.setMrp(productBatch.getMrp());
    productBatchEntity.setProductId(productBatch.getProductId());
    productBatchEntity.setUpdatedBy(productBatch.getUpdatedBy());
    productBatchEntity.setUpdatedOn(productBatch.getUpdatedOn());
    return productBatchEntity;
  }

  public static List<ProductBatchLite> mapEntityToLite(
      List<ProductBatchEntity> productBatchEntityList) {
    List<ProductBatchLite> productBatchLites = new ArrayList<>();
    productBatchEntityList.forEach(r -> productBatchLites.add(mapEntityToLite(r)));
    return productBatchLites;
  }

  private static ProductBatchLite mapEntityToLite(ProductBatchEntity productBatchEntity) {
    ProductBatchLite productBatchLite = new ProductBatchLite();
    productBatchLite.setAvailableQuantity(productBatchEntity.getAvailableQuantity());
    productBatchLite.setBatchNo(productBatchEntity.getBatchNo());
    productBatchLite.setExpiryDate(productBatchEntity.getExpiryDate());
    productBatchLite.setId(productBatchEntity.getId());
    productBatchLite.setInitialQuantity(productBatchEntity.getInitialQuantity());
    productBatchLite.setIsActive(productBatchEntity.getIsActive());
    productBatchLite.setMrp(productBatchEntity.getMrp());
    productBatchLite.setProductId(productBatchEntity.getProductId());
    return productBatchLite;
  }

}
