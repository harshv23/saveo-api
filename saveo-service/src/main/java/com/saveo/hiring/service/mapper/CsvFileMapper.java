package com.saveo.hiring.service.mapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.model.csv.CsvFileModel;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class CsvFileMapper {

  private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

  public static CsvFileModel mapCsvRowToModel(String[] csvRow) throws SaveoBusinessException {
    CsvFileModel csvFileModel = new CsvFileModel();

    try {
      csvFileModel.setBalanceQuantity(Integer.parseInt(csvRow[3]));

      csvFileModel.setBatchNo(csvRow[1]);
      csvFileModel.setExpiryDate(dateFormat.parse(csvRow[2]));
      csvFileModel.setHsnCode(csvRow[9]);
      csvFileModel.setManufacturerName(csvRow[8]);
      csvFileModel.setMedicineName(csvRow[0]);
      csvFileModel.setMrp(Double.parseDouble(csvRow[7]));
      csvFileModel.setPackaging(csvRow[4]);
      csvFileModel.setProductCode(csvRow[5]);
      csvFileModel.setSchemes(csvRow[6]);
      return csvFileModel;
    } catch (Exception e) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.INVALID_VALUES_CSV.getErrorCode(),
          SaveoErrorCodeEnum.INVALID_VALUES_CSV.getErrorMessage());
    }
  }

}
