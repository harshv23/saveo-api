package com.saveo.hiring.service.interfaces;

import java.util.List;
import java.util.Set;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.model.ProductLite;
import com.saveo.hiring.service.model.Product;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IProductService {

  Product getProductByCode(String productCode) throws SaveoBusinessException;

  Product saveProduct(Product product) throws SaveoBusinessException;

  List<ProductLite> getMatchingProductByName(String name) throws SaveoBusinessException;

  Product getProductById(Long productId) throws SaveoBusinessException;

  List<Product> getProductByIdSet(Set<Long> idSet) throws SaveoBusinessException;

}
