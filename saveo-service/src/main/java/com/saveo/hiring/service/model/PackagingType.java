package com.saveo.hiring.service.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class PackagingType implements Serializable {


  /**
   *
   */
  private static final long serialVersionUID = -1848189439958942327L;
  private Long id;
  private String name;
  private Date createdOn;
  private String createdBy;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public String toString() {
    return "PackagingType [id=" + id + ", name=" + name + ", createdOn=" + createdOn
        + ", createdBy=" + createdBy + "]";
  }

}
