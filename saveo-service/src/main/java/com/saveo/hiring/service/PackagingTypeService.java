package com.saveo.hiring.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.constant.SaveoConstants;
import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.dao.model.PackagingTypeEntity;
import com.saveo.hiring.dao.repository.IPackagingTypeRepository;
import com.saveo.hiring.service.interfaces.IPackagingTypeService;
import com.saveo.hiring.service.mapper.PackagingTypeMapper;
import com.saveo.hiring.service.model.PackagingType;

@Service
public class PackagingTypeService implements IPackagingTypeService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IPackagingTypeRepository packagingTypeRepository;

  @Override
  public Map<String, PackagingType> getPackagingTypeMap() throws SaveoBusinessException {
    logger.debug("Request received.");

    Map<String, PackagingType> packagingTypeMap = new HashMap<>();

    List<PackagingTypeEntity> packagingTypeEntityList = null;
    try {
      packagingTypeEntityList = (List<PackagingTypeEntity>) packagingTypeRepository.findAll();
    } catch (Exception e) {
      logger.error("Error occurred while fetching all packaging types from db .", e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (packagingTypeEntityList != null) {
      return PackagingTypeMapper.mapEntityListToMap(packagingTypeEntityList);
    }
    return packagingTypeMap;

  }

  @Override
  public PackagingType savePackagingType(String packaging) throws SaveoBusinessException {
    logger.debug("Request received with packaging: " + packaging);

    PackagingTypeEntity packagingTypeEntity = new PackagingTypeEntity();
    packagingTypeEntity.setCreatedBy(SaveoConstants.CREATED_BY);
    packagingTypeEntity.setCreatedOn(new Date());
    packagingTypeEntity.setName(packaging);

    try {
      packagingTypeEntity = packagingTypeRepository.save(packagingTypeEntity);
      return PackagingTypeMapper.mapEntityToPackagingType(packagingTypeEntity);
    } catch (Exception e) {
      logger.error("Error occurred while saving packaging : " + packaging, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DATABASE_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

  }

  @Override
  public Map<Long, PackagingType> getPackagingTypeIdMap() throws SaveoBusinessException {

    logger.debug("Request received.");

    Map<Long, PackagingType> packagingTypeMap = new HashMap<>();

    List<PackagingTypeEntity> packagingTypeEntityList = null;
    try {
      packagingTypeEntityList = (List<PackagingTypeEntity>) packagingTypeRepository.findAll();
    } catch (Exception e) {
      logger.error("Error occurred while fetching all packaging types from db .", e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (packagingTypeEntityList != null) {
      return PackagingTypeMapper.mapEntityListToIdMap(packagingTypeEntityList);
    }
    return packagingTypeMap;


  }

  @Override
  public PackagingType getPackagingTypeById(Long packagingTypeId) throws SaveoBusinessException {
    logger.debug("Request received with packagingTypeId: " + packagingTypeId);

    try {
      Optional<PackagingTypeEntity> packagingTypeOptional =
          packagingTypeRepository.findById(packagingTypeId);
      if (!packagingTypeOptional.isPresent()) {
        throw new SaveoBusinessException(SaveoErrorCodeEnum.PACKAGING_TYPE_NOT_FOUND.getErrorCode(),
            SaveoErrorCodeEnum.PACKAGING_TYPE_NOT_FOUND.getErrorMessage());
      }

      return PackagingTypeMapper.mapEntityToPackagingType(packagingTypeOptional.get());
    } catch (SaveoBusinessException e) {
      throw e;
    } catch (Exception e) {
      logger.error(
          "Error occurred while fetching packagint type from db with id: " + packagingTypeId, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }
  }

}
