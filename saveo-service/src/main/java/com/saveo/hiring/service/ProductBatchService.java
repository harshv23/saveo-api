package com.saveo.hiring.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.model.ProductBatchLite;
import com.saveo.hiring.dao.model.ProductBatchEntity;
import com.saveo.hiring.dao.repository.IProductBatchRepository;
import com.saveo.hiring.service.interfaces.IProductBatchService;
import com.saveo.hiring.service.mapper.ProductBatchMapper;
import com.saveo.hiring.service.model.ProductBatch;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
@Service
public class ProductBatchService implements IProductBatchService {

  private static final Boolean isActive = true;

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IProductBatchRepository productBatchRepository;

  @Override
  public Long saveProductBatch(ProductBatch productBatch) throws SaveoBusinessException {
    logger.debug("Request received with productBatch: " + productBatch);

    ProductBatchEntity productBatchEntity =
        ProductBatchMapper.mapProductBatchToEntity(productBatch);
    try {
      return productBatchRepository.save(productBatchEntity).getId();
    } catch (Exception e) {
      logger.error("Error occurred while saving product batch with productBatchEntity: "
          + productBatchEntity, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DATABASE_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

  }

  @Override
  public List<ProductBatchLite> getProductBatchLiteListByProductId(Long productId)
      throws SaveoBusinessException {
    logger.debug("Request received with productId: " + productId);

    List<ProductBatchEntity> productBatchEntityList = null;

    try {
      productBatchEntityList =
          productBatchRepository.getProductBatchByProductIdAndIsActive(productId, isActive);
    } catch (Exception e) {
      logger.error("Error occurred while fetching product batch by product id: " + productId, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (productBatchEntityList != null) {
      return ProductBatchMapper.mapEntityToLite(productBatchEntityList);
    }

    return null;
  }

}
