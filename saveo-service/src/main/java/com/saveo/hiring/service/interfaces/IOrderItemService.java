package com.saveo.hiring.service.interfaces;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.model.OrderItem;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IOrderItemService {

  Long saveOrderItem(OrderItem orderItem) throws SaveoBusinessException;

}
