package com.saveo.hiring.service.mapper;

import com.saveo.hiring.dao.model.OrderItemEntity;
import com.saveo.hiring.service.model.OrderItem;

public class OrderItemMapper {

  public static OrderItemEntity mapOrderItemToEntity(OrderItem orderItem) {
    OrderItemEntity orderItemEntity = new OrderItemEntity();
    orderItemEntity.setCreatedOn(orderItem.getCreatedOn());
    orderItemEntity.setId(orderItem.getId());
    orderItemEntity.setIsActive(orderItem.getIsActive());
    orderItemEntity.setOrderId(orderItem.getOrderId());
    orderItemEntity.setProductId(orderItem.getProductId());
    orderItemEntity.setQuantity(orderItem.getQuantity());

    return orderItemEntity;
  }

}
