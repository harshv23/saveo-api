package com.saveo.hiring.service.mapper;

import com.saveo.hiring.dao.model.OrderEntity;
import com.saveo.hiring.service.model.Order;

public class OrderMapper {

  public static OrderEntity mapOrderToEntity(Order order) {
    OrderEntity orderEntity = new OrderEntity();
    orderEntity.setCreatedOn(order.getCreatedOn());
    orderEntity.setId(order.getId());
    orderEntity.setOrderNo(order.getOrderNo());
    return orderEntity;
  }

  public static Order mapEntityToOrder(OrderEntity order) {
    Order orderEntity = new Order();
    orderEntity.setCreatedOn(order.getCreatedOn());
    orderEntity.setId(order.getId());
    orderEntity.setOrderNo(order.getOrderNo());
    return orderEntity;
  }

}
