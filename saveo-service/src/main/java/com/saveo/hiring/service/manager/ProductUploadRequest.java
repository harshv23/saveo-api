package com.saveo.hiring.service.manager;

import java.io.Serializable;
import java.util.Map;

import com.saveo.hiring.service.model.Product;
import com.saveo.hiring.service.model.ProductBatch;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class ProductUploadRequest implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -4782571827812461976L;

  private Product product;
  private Map<String, ProductBatch> batchNoProductBatchMap;

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Map<String, ProductBatch> getBatchNoProductBatchMap() {
    return batchNoProductBatchMap;
  }

  public void setBatchNoProductBatchMap(Map<String, ProductBatch> batchNoProductBatchMap) {
    this.batchNoProductBatchMap = batchNoProductBatchMap;
  }

  @Override
  public String toString() {
    return "ProductUploadRequest [product=" + product + ", batchNoProductBatchMap="
        + batchNoProductBatchMap + "]";
  }

}
