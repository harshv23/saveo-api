package com.saveo.hiring.service.helper;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.interfaces.IProductBatchService;
import com.saveo.hiring.service.interfaces.IProductService;
import com.saveo.hiring.service.manager.ProductUploadRequest;
import com.saveo.hiring.service.model.Product;
import com.saveo.hiring.service.model.ProductBatch;

@Service
public class ProductHelperService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IProductService productService;

  @Autowired
  private IProductBatchService productBatchService;

  public Boolean saveProductWithBatches(
      Map<String, ProductUploadRequest> productCodeProductUploadRequestMap)
      throws SaveoBusinessException {
    logger.debug("Request received with productCodeProductUploadRequestMap: "
        + productCodeProductUploadRequestMap);

    for (Map.Entry<String, ProductUploadRequest> entry : productCodeProductUploadRequestMap
        .entrySet()) {
      String productCode = entry.getKey();

      Product product = productService.getProductByCode(productCode);

      if (product == null) {
        // create product
        product = productService.saveProduct(entry.getValue().getProduct());
      }

      Map<String, ProductBatch> batchNoProductBatchMap =
          entry.getValue().getBatchNoProductBatchMap();

      for (Map.Entry<String, ProductBatch> batchEntry : batchNoProductBatchMap.entrySet()) {
        ProductBatch productBatch = batchEntry.getValue();
        productBatch.setProductId(product.getId());

        productBatchService.saveProductBatch(productBatch);
      }

    }

    return true;
  }

}
