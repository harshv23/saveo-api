package com.saveo.hiring.service.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class Product implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -8492802829412351909L;

  private Long id;
  private String name;
  private String code;
  private String hsnCode;
  private Long manufacturerId;
  private Long packagingTypeId;
  private Boolean isActive;
  private Date createdOn;
  private Timestamp updatedOn;
  private String createdBy;
  private String updatedBy;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getHsnCode() {
    return hsnCode;
  }

  public void setHsnCode(String hsnCode) {
    this.hsnCode = hsnCode;
  }

  public Long getManufacturerId() {
    return manufacturerId;
  }

  public void setManufacturerId(Long manufacturerId) {
    this.manufacturerId = manufacturerId;
  }

  public Long getPackagingTypeId() {
    return packagingTypeId;
  }

  public void setPackagingTypeId(Long packagingTypeId) {
    this.packagingTypeId = packagingTypeId;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Timestamp getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(Timestamp updatedOn) {
    this.updatedOn = updatedOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public String toString() {
    return "Product [id=" + id + ", name=" + name + ", code=" + code + ", hsnCode=" + hsnCode
        + ", manufacturerId=" + manufacturerId + ", packagingTypeId=" + packagingTypeId
        + ", isActive=" + isActive + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn
        + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + "]";
  }

}
