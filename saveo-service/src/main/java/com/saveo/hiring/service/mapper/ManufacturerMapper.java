package com.saveo.hiring.service.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.saveo.hiring.dao.model.ManufacturerEntity;
import com.saveo.hiring.service.model.Manufacturer;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class ManufacturerMapper {

  public static Map<String, Manufacturer> mapEntityListToMap(
      List<ManufacturerEntity> manufacturerEntityList) {
    Map<String, Manufacturer> map = new HashMap<>();

    manufacturerEntityList.forEach(r -> map.put(r.getName(), mapEntityToManufacturer(r)));

    return map;
  }

  public static Manufacturer mapEntityToManufacturer(ManufacturerEntity manufacturerEntity) {
    Manufacturer manufacturer = new Manufacturer();
    manufacturer.setCreatedBy(manufacturerEntity.getCreatedBy());
    manufacturer.setCreatedOn(manufacturerEntity.getCreatedOn());
    manufacturer.setId(manufacturerEntity.getId());
    manufacturer.setIsActive(manufacturerEntity.getIsActive());
    manufacturer.setName(manufacturerEntity.getName());
    manufacturer.setUpdatedBy(manufacturerEntity.getUpdatedBy());
    manufacturer.setUpdatedOn(manufacturerEntity.getUpdatedOn());
    return manufacturer;
  }

  public static Map<Long, Manufacturer> mapEntityListToIdMap(
      List<ManufacturerEntity> manufacturerEntityList) {

    Map<Long, Manufacturer> map = new HashMap<>();

    manufacturerEntityList.forEach(r -> map.put(r.getId(), mapEntityToManufacturer(r)));

    return map;

  }

}
