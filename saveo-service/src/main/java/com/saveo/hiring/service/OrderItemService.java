package com.saveo.hiring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.dao.model.OrderItemEntity;
import com.saveo.hiring.dao.repository.IOrderItemRepository;
import com.saveo.hiring.service.interfaces.IOrderItemService;
import com.saveo.hiring.service.mapper.OrderItemMapper;
import com.saveo.hiring.service.model.OrderItem;

@Service
public class OrderItemService implements IOrderItemService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IOrderItemRepository orderItemRepository;

  @Override
  public Long saveOrderItem(OrderItem orderItem) throws SaveoBusinessException {
    logger.debug("Request received with orderItem: " + orderItem);

    OrderItemEntity orderItemEntity = OrderItemMapper.mapOrderItemToEntity(orderItem);

    try {
      return orderItemRepository.save(orderItemEntity).getId();
    } catch (Exception e) {
      logger.error("Error occurred while saving order item, orderItem: " + orderItem, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DATABASE_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }
  }

}
