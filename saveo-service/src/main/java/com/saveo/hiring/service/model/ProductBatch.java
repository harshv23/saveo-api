package com.saveo.hiring.service.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class ProductBatch implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -3481634715415464079L;
  private Long id;
  private String batchNo;
  private Date expiryDate;
  private Long productId;
  private Double mrp;
  private Integer initialQuantity;
  private Integer availableQuantity;
  private Boolean isActive;
  private Date createdOn;
  private Timestamp updatedOn;
  private String createdBy;
  private String updatedBy;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Double getMrp() {
    return mrp;
  }

  public void setMrp(Double mrp) {
    this.mrp = mrp;
  }

  public Integer getInitialQuantity() {
    return initialQuantity;
  }

  public void setInitialQuantity(Integer initialQuantity) {
    this.initialQuantity = initialQuantity;
  }

  public Integer getAvailableQuantity() {
    return availableQuantity;
  }

  public void setAvailableQuantity(Integer availableQuantity) {
    this.availableQuantity = availableQuantity;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Timestamp getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(Timestamp updatedOn) {
    this.updatedOn = updatedOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public String toString() {
    return "ProductBatch [id=" + id + ", batchNo=" + batchNo + ", expiryDate=" + expiryDate
        + ", productId=" + productId + ", mrp=" + mrp + ", initialQuantity=" + initialQuantity
        + ", availableQuantity=" + availableQuantity + ", isActive=" + isActive + ", createdOn="
        + createdOn + ", updatedOn=" + updatedOn + ", createdBy=" + createdBy + ", updatedBy="
        + updatedBy + "]";
  }

}
