package com.saveo.hiring.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.constant.SaveoConstants;
import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.dao.model.ManufacturerEntity;
import com.saveo.hiring.dao.repository.IManufacturerRepository;
import com.saveo.hiring.service.interfaces.IManufacturerService;
import com.saveo.hiring.service.mapper.ManufacturerMapper;
import com.saveo.hiring.service.model.Manufacturer;

@Service
public class ManufacturerService implements IManufacturerService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IManufacturerRepository manufacturerRepository;

  @Override
  public Map<String, Manufacturer> getManufacturerMap() throws SaveoBusinessException {
    logger.debug("Request received ");

    Map<String, Manufacturer> manufacturerMap = new HashMap<>();

    List<ManufacturerEntity> manufacturerEntityList = null;
    try {
      manufacturerEntityList = (List<ManufacturerEntity>) manufacturerRepository.findAll();
    } catch (Exception e) {
      logger.error("Error occurred while fetching all manufacturers from db .", e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (manufacturerEntityList != null) {
      return ManufacturerMapper.mapEntityListToMap(manufacturerEntityList);
    }
    return manufacturerMap;
  }

  @Override
  public Manufacturer saveManufacturer(String manufacturerName) throws SaveoBusinessException {
    logger.debug("Request received with manufacturerName: " + manufacturerName);

    ManufacturerEntity manufacturerEntity = new ManufacturerEntity();
    manufacturerEntity.setCreatedBy(SaveoConstants.CREATED_BY);
    manufacturerEntity.setCreatedOn(new Date());
    manufacturerEntity.setIsActive(true);
    manufacturerEntity.setName(manufacturerName);


    try {
      manufacturerEntity = manufacturerRepository.save(manufacturerEntity);
      return ManufacturerMapper.mapEntityToManufacturer(manufacturerEntity);
    } catch (Exception e) {
      logger.error("Error occurred while saving manufacturer : " + manufacturerName, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DATABASE_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

  }

  @Override
  public Map<Long, Manufacturer> getManufacturerIdMap() throws SaveoBusinessException {

    logger.debug("Request received ");

    Map<Long, Manufacturer> manufacturerMap = new HashMap<>();

    List<ManufacturerEntity> manufacturerEntityList = null;
    try {
      manufacturerEntityList = (List<ManufacturerEntity>) manufacturerRepository.findAll();
    } catch (Exception e) {
      logger.error("Error occurred while fetching all manufacturers from db .", e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (manufacturerEntityList != null) {
      return ManufacturerMapper.mapEntityListToIdMap(manufacturerEntityList);
    }
    return manufacturerMap;

  }

  @Override
  public Manufacturer getManufacturerById(Long manufacturerId) throws SaveoBusinessException {
    logger.debug("Request received with manufacturerId: " + manufacturerId);

    try {
      Optional<ManufacturerEntity> manufacturerOptional =
          manufacturerRepository.findById(manufacturerId);

      if (!manufacturerOptional.isPresent()) {
        throw new SaveoBusinessException(SaveoErrorCodeEnum.MANUFACTURER_NOT_FOUND.getErrorCode(),
            SaveoErrorCodeEnum.MANUFACTURER_NOT_FOUND.getErrorMessage());
      }

      return ManufacturerMapper.mapEntityToManufacturer(manufacturerOptional.get());
    } catch (SaveoBusinessException e) {
      throw e;
    } catch (Exception e) {
      logger.error("Error occurred while fetching manufacturer from db with id: " + manufacturerId,
          e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

  }

}
