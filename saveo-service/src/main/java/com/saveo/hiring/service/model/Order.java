package com.saveo.hiring.service.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class Order implements Serializable {


  /**
   *
   */
  private static final long serialVersionUID = 1350838764676903699L;

  private Long id;
  private String orderNo;
  private Date createdOn;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }


  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  @Override
  public String toString() {
    return "Order [id=" + id + ", orderNo=" + orderNo + ", createdOn=" + createdOn + "]";
  }

}
