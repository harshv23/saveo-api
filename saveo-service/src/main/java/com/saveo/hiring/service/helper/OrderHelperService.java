package com.saveo.hiring.service.helper;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.interfaces.IOrderService;
import com.saveo.hiring.service.model.Order;
import com.saveo.hiring.service.model.OrderItem;
import com.saveo.hiring.service.interfaces.IOrderItemService;
import com.saveo.hiring.common.model.OrderProduct;
import com.saveo.hiring.common.model.PlaceOrderResponse;

@Service
public class OrderHelperService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IOrderItemService orderItemService;

  @Autowired
  private IOrderService orderService;

  public PlaceOrderResponse placeOrder(List<OrderProduct> orderProductList)
      throws SaveoBusinessException {
    logger.debug("Request received with orderProductList: " + orderProductList);

    Order order = new Order();
    order.setCreatedOn(new Date());
    order.setOrderNo(UUID.randomUUID().toString());
    order = orderService.saveOrder(order);

    // proceed to save items

    for (OrderProduct orderProduct : orderProductList) {

      OrderItem orderItem = new OrderItem();
      orderItem.setCreatedOn(new Date());
      orderItem.setIsActive(true);
      orderItem.setOrderId(order.getId());
      orderItem.setProductId(orderProduct.getC_unique_id());
      orderItem.setQuantity(orderProduct.getQuantity());

      orderItemService.saveOrderItem(orderItem);

    }

    PlaceOrderResponse placeOrderResponse = new PlaceOrderResponse();
    placeOrderResponse.setIsSuccess(true);
    placeOrderResponse.setOrderNo(order.getOrderNo());
    return placeOrderResponse;
  }

}
