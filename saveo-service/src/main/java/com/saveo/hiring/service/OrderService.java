package com.saveo.hiring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.dao.model.OrderEntity;
import com.saveo.hiring.dao.repository.IOrderRepository;
import com.saveo.hiring.service.interfaces.IOrderService;
import com.saveo.hiring.service.mapper.OrderMapper;
import com.saveo.hiring.service.model.Order;

@Service
public class OrderService implements IOrderService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IOrderRepository orderRepository;

  @Override
  public Order saveOrder(Order order) throws SaveoBusinessException {
    logger.debug("Request received with order: " + order);

    OrderEntity orderEntity = OrderMapper.mapOrderToEntity(order);
    try {
      orderEntity = orderRepository.save(orderEntity);
      if (orderEntity != null) {
        return OrderMapper.mapEntityToOrder(orderEntity);
      }
    } catch (Exception e) {
      logger.error("Error occurred while saving order into db , order: " + orderEntity, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DATABASE_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }
    return null;
  }

}
