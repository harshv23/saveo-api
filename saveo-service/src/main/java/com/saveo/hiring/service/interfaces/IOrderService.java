package com.saveo.hiring.service.interfaces;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.model.Order;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IOrderService {

  Order saveOrder(Order order) throws SaveoBusinessException;

}
