package com.saveo.hiring.service.mapper;

import java.util.ArrayList;
import java.util.List;

import com.saveo.hiring.common.model.ProductLite;
import com.saveo.hiring.dao.model.ProductEntity;
import com.saveo.hiring.service.model.Product;

public class ProductMapper {

  public static Product mapEntityToProduct(ProductEntity productEntity) {
    Product product = new Product();
    product.setCode(productEntity.getCode());
    product.setCreatedBy(productEntity.getCreatedBy());
    product.setCreatedOn(productEntity.getCreatedOn());
    product.setHsnCode(productEntity.getHsnCode());
    product.setId(productEntity.getId());
    product.setIsActive(productEntity.getIsActive());
    product.setManufacturerId(productEntity.getManufacturerId());
    product.setName(productEntity.getName());
    product.setUpdatedBy(productEntity.getUpdatedBy());

    product.setPackagingTypeId(productEntity.getPackagingTypeId());
    product.setUpdatedOn(productEntity.getUpdatedOn());

    return product;
  }

  public static ProductEntity mapProductToEntity(Product productEntity) {
    ProductEntity product = new ProductEntity();
    product.setCode(productEntity.getCode());
    product.setCreatedBy(productEntity.getCreatedBy());
    product.setCreatedOn(productEntity.getCreatedOn());
    product.setHsnCode(productEntity.getHsnCode());
    product.setId(productEntity.getId());
    product.setIsActive(productEntity.getIsActive());
    product.setManufacturerId(productEntity.getManufacturerId());
    product.setName(productEntity.getName());
    product.setUpdatedBy(productEntity.getUpdatedBy());

    product.setPackagingTypeId(productEntity.getPackagingTypeId());
    product.setUpdatedOn(productEntity.getUpdatedOn());

    return product;
  }

  public static List<ProductLite> mapEntityToLite(List<ProductEntity> productEntityList) {
    List<ProductLite> productLiteList = new ArrayList<>();

    productEntityList.forEach(r -> productLiteList.add(mapEntityToLite(r)));

    return productLiteList;
  }

  public static ProductLite mapEntityToLite(ProductEntity productEntity) {
    ProductLite productLite = new ProductLite();
    productLite.setProductId(productEntity.getId());
    productLite.setProductName(productEntity.getName());
    return productLite;
  }

  public static List<Product> mapEntityToProduct(List<ProductEntity> productEntityList) {
    List<Product> products = new ArrayList<>();
    productEntityList.forEach(r -> products.add(mapEntityToProduct(r)));
    return products;
  }


}
