package com.saveo.hiring.service.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;
import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.mapper.CsvFileMapper;
import com.saveo.hiring.service.model.csv.CsvFileModel;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class FileUtils {

  public static String TYPE = "text/csv";
  static String[] HEADER_LIST = { "c_name", "c_manufacturer", "c_batch_no", "d_expiry_date",
      "n_balance_qty", "c_packaging", "c_unique_code", "c_schemes", "n_mrp", "hsn_code" };

  public static boolean hasCSVFormat(MultipartFile file) {

    if (!TYPE.equals(file.getContentType())) {
      return false;
    }

    return true;
  }

  public static List<CsvFileModel> parseDataFromCSV(InputStream csvInputStream)
      throws SaveoBusinessException {

    CSVReader reader = null;
    try {
      reader = new CSVReader(new InputStreamReader(csvInputStream));
      List<String[]> csvRowList = reader.readAll();
      csvRowList.remove(0);
      List<CsvFileModel> csvFileModelList = new ArrayList<>();
      for (String[] row : csvRowList) {
        csvFileModelList.add(CsvFileMapper.mapCsvRowToModel(row));
      }

      return csvFileModelList;
    } catch (IOException e) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.INVALID_FILE_FORMAT.getErrorCode(),
          SaveoErrorCodeEnum.INVALID_FILE_FORMAT.getErrorMessage());
    } catch (Exception e) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorCode(),
          SaveoErrorCodeEnum.INTERNAL_ERROR.getErrorMessage());
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          // TODO
        }
      }

    }

  }

}
