package com.saveo.hiring.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.model.ProductLite;
import com.saveo.hiring.dao.model.ProductEntity;
import com.saveo.hiring.dao.repository.IProductRepository;
import com.saveo.hiring.service.interfaces.IProductService;
import com.saveo.hiring.service.mapper.ProductMapper;
import com.saveo.hiring.service.model.Product;

@Service
public class ProductService implements IProductService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IProductRepository productRepository;


  @Override
  public Product getProductByCode(String code) throws SaveoBusinessException {
    logger.debug("Request received with code: " + code);

    ProductEntity productEntity = null;

    try {
      productEntity = productRepository.getProductByCode(code);
    } catch (Exception e) {
      logger.error("Error occurred while fetching product from db with code: " + code, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (productEntity != null) {
      return ProductMapper.mapEntityToProduct(productEntity);
    }
    return null;
  }


  @Override
  public Product saveProduct(Product product) throws SaveoBusinessException {
    logger.debug("Request received with product: " + product);

    ProductEntity productEntity = ProductMapper.mapProductToEntity(product);
    try {
      productEntity = productRepository.save(productEntity);
      return ProductMapper.mapEntityToProduct(productEntity);
    } catch (Exception e) {
      logger.error("Error occurred while saving product to db.", e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DATABASE_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

  }


  @Override
  public List<ProductLite> getMatchingProductByName(String name) throws SaveoBusinessException {
    logger.debug("Request received wiht name: " + name);

    List<ProductEntity> productEntityList = null;

    try {
      productEntityList = productRepository.findByNameContaining(name);
    } catch (Exception e) {
      logger.error("Error occurred while fetching products from db containing name : " + name, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }
    if (productEntityList != null) {
      return ProductMapper.mapEntityToLite(productEntityList);
    }

    return null;
  }


  @Override
  public Product getProductById(Long productId) throws SaveoBusinessException {
    logger.debug("Request received with productId: " + productId);

    try {
      Optional<ProductEntity> productOptional = productRepository.findById(productId);
      if (!productOptional.isPresent()) {
        throw new SaveoBusinessException(SaveoErrorCodeEnum.NO_PRODUCTS_FOUND.getErrorCode(),
            SaveoErrorCodeEnum.NO_PRODUCTS_FOUND.getErrorMessage());
      }

      return ProductMapper.mapEntityToProduct(productOptional.get());
    } catch (SaveoBusinessException e) {
      throw e;
    } catch (Exception e) {
      logger.error("Error occurred while fetching product with id: " + productId, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }
  }


  @Override
  public List<Product> getProductByIdSet(Set<Long> idSet) throws SaveoBusinessException {
    logger.debug("Request received with idSet: " + idSet);

    List<ProductEntity> productEntityList = null;
    try {
      productEntityList = productRepository.getProductByIdSet(idSet);
    } catch (Exception e) {
      logger.error("Error occurred while fetching product with id in " + idSet, e);
      throw new SaveoBusinessException(SaveoErrorCodeEnum.DB_FETCH_ERROR.getErrorCode(),
          e.getCause().getCause().getMessage());
    }

    if (productEntityList != null) {
      return ProductMapper.mapEntityToProduct(productEntityList);
    }
    return null;
  }

}
