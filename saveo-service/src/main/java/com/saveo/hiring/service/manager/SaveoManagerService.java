package com.saveo.hiring.service.manager;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saveo.hiring.common.constant.SaveoConstants;
import com.saveo.hiring.common.enums.SaveoErrorCodeEnum;
import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.interfaces.ISaveoManagerService;
import com.saveo.hiring.common.model.GetProductResponse;
import com.saveo.hiring.common.model.OrderProduct;
import com.saveo.hiring.common.model.PlaceOrderResponse;
import com.saveo.hiring.common.model.ProductBatchLite;
import com.saveo.hiring.common.model.ProductLite;
import com.saveo.hiring.common.model.ProductResponse;
import com.saveo.hiring.common.model.SearchMedicineResponse;
import com.saveo.hiring.common.model.UploadCsvResponse;
import com.saveo.hiring.service.helper.OrderHelperService;
import com.saveo.hiring.service.helper.ProductHelperService;
import com.saveo.hiring.service.interfaces.IManufacturerService;
import com.saveo.hiring.service.interfaces.IPackagingTypeService;
import com.saveo.hiring.service.interfaces.IProductBatchService;
import com.saveo.hiring.service.interfaces.IProductService;
import com.saveo.hiring.service.model.Manufacturer;
import com.saveo.hiring.service.model.PackagingType;
import com.saveo.hiring.service.model.Product;
import com.saveo.hiring.service.model.ProductBatch;
import com.saveo.hiring.service.model.csv.CsvFileModel;
import com.saveo.hiring.service.util.FileUtils;


/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
@Service
@Transactional
public class SaveoManagerService implements ISaveoManagerService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private IPackagingTypeService packagingTypeService;

  @Autowired
  private IManufacturerService manufacturerService;

  @Autowired
  private ProductHelperService productHelperService;

  @Autowired
  private IProductService productService;

  @Autowired
  private IProductBatchService productBatchService;

  @Autowired
  private OrderHelperService orderHelperService;

  @Override
  public UploadCsvResponse uploadCsv(InputStream csvInputStream) throws SaveoBusinessException {

    List<CsvFileModel> csvFileModelList = FileUtils.parseDataFromCSV(csvInputStream);

    // Now the data is extracted and validated. proceed to save data
    Map<String, Manufacturer> manufacturerNameIdMap = manufacturerService.getManufacturerMap();
    Map<String, PackagingType> packagingTypeMap = packagingTypeService.getPackagingTypeMap();

    Map<String, ProductUploadRequest> productCodeProductUploadRequestMap = new HashMap<>();
    for (CsvFileModel csvFileModel : csvFileModelList) {

      if (packagingTypeMap.get(csvFileModel.getPackaging()) == null) {

        PackagingType packagingType =
            packagingTypeService.savePackagingType(csvFileModel.getPackaging());
        packagingTypeMap.put(csvFileModel.getPackaging(), packagingType);
      }

      if (manufacturerNameIdMap.get(csvFileModel.getManufacturerName()) == null) {
        Manufacturer manufacturer =
            manufacturerService.saveManufacturer(csvFileModel.getManufacturerName());
        manufacturerNameIdMap.put(csvFileModel.getManufacturerName(), manufacturer);
      }

      if (productCodeProductUploadRequestMap.get(csvFileModel.getProductCode()) == null) {
        ProductUploadRequest productUploadRequest = new ProductUploadRequest();
        Product product = new Product();
        product.setCode(csvFileModel.getProductCode());
        product.setCreatedBy(SaveoConstants.CREATED_BY);
        product.setCreatedOn(new Date());
        product.setHsnCode(csvFileModel.getHsnCode());
        product.setIsActive(true);
        product.setManufacturerId(
            manufacturerNameIdMap.get(csvFileModel.getManufacturerName()).getId());
        product.setName(csvFileModel.getMedicineName());
        product.setPackagingTypeId(packagingTypeMap.get(csvFileModel.getPackaging()).getId());
        productUploadRequest.setProduct(product);
        productUploadRequest.setBatchNoProductBatchMap(new HashMap<>());
        productCodeProductUploadRequestMap.put(csvFileModel.getProductCode(), productUploadRequest);

      }

      Map<String, ProductBatch> batchNoProductBatchMap = productCodeProductUploadRequestMap
          .get(csvFileModel.getProductCode()).getBatchNoProductBatchMap();

      if (batchNoProductBatchMap.get(csvFileModel.getBatchNo()) == null) {
        ProductBatch productBatch = new ProductBatch();
        productBatch.setAvailableQuantity(csvFileModel.getBalanceQuantity());
        productBatch.setBatchNo(csvFileModel.getBatchNo());
        productBatch.setCreatedBy(SaveoConstants.CREATED_BY);
        productBatch.setCreatedOn(new Date());
        productBatch.setExpiryDate(csvFileModel.getExpiryDate());
        productBatch.setInitialQuantity(csvFileModel.getBalanceQuantity());
        productBatch.setIsActive(true);
        productBatch.setMrp(csvFileModel.getMrp());
        batchNoProductBatchMap.put(csvFileModel.getBatchNo(), productBatch);
      } else {
        batchNoProductBatchMap.get(csvFileModel.getBatchNo()).setAvailableQuantity(
            batchNoProductBatchMap.get(csvFileModel.getBatchNo()).getAvailableQuantity()
                + csvFileModel.getBalanceQuantity());
      }

    }

    Boolean response =
        productHelperService.saveProductWithBatches(productCodeProductUploadRequestMap);

    UploadCsvResponse uploadCsvResponse = new UploadCsvResponse();
    uploadCsvResponse.setIsSuccess(response);
    return uploadCsvResponse;
  }

  @Override
  public SearchMedicineResponse getProductListWithName(String name) throws SaveoBusinessException {
    logger.debug("Reqeust received with name: " + name);

    List<ProductLite> productLiteList = productService.getMatchingProductByName(name);

    SearchMedicineResponse searchMedicineResponse = new SearchMedicineResponse();
    if ((productLiteList != null) && !productLiteList.isEmpty()) {
      searchMedicineResponse.setIsSuccess(true);
      searchMedicineResponse.setProductLiteList(productLiteList);
      return searchMedicineResponse;
    } else {
      searchMedicineResponse.setIsSuccess(true);
      searchMedicineResponse.setErrorCode(SaveoErrorCodeEnum.NO_PRODUCTS_FOUND.getErrorCode());
      searchMedicineResponse
          .setErrorMessage(SaveoErrorCodeEnum.NO_PRODUCTS_FOUND.getErrorMessage());
      searchMedicineResponse.setProductLiteList(new ArrayList<ProductLite>());
      return searchMedicineResponse;
    }
  }

  @Override
  public GetProductResponse getProductById(Long productId) throws SaveoBusinessException {
    logger.debug("Request received with productId: " + productId);

    Product product = productService.getProductById(productId);

    Manufacturer manufacturer =
        manufacturerService.getManufacturerById(product.getManufacturerId());

    PackagingType packagingType =
        packagingTypeService.getPackagingTypeById(product.getPackagingTypeId());

    List<ProductBatchLite> productBatchLiteList =
        productBatchService.getProductBatchLiteListByProductId(productId);

    ProductResponse productResponse = new ProductResponse();
    productResponse.setCode(product.getCode());
    productResponse.setCreatedBy(product.getCreatedBy());
    productResponse.setCreatedOn(product.getCreatedOn());
    productResponse.setHsnCode(product.getHsnCode());
    productResponse.setId(product.getId());
    productResponse.setIsActive(product.getIsActive());
    productResponse.setManufacturerId(product.getManufacturerId());
    productResponse.setManufacturerName(manufacturer.getName());
    productResponse.setName(product.getName());
    productResponse.setPackagingTypeId(packagingType.getId());
    productResponse.setPackagingTypeName(packagingType.getName());
    productResponse.setProductBatchList(productBatchLiteList);
    productResponse.setUpdatedBy(product.getUpdatedBy());
    productResponse.setUpdatedOn(product.getUpdatedOn());


    GetProductResponse getProductResponse = new GetProductResponse();
    getProductResponse.setIsSuccess(true);
    getProductResponse.setProductResponse(productResponse);
    return getProductResponse;
  }

  @Override
  public PlaceOrderResponse placeOrder(List<OrderProduct> orderProductList)
      throws SaveoBusinessException {
    logger.debug("Request received with orderProductList: " + orderProductList);

    Map<Long, OrderProduct> productIdOrderProductMap = orderProductList.stream()
        .filter(Objects::nonNull).collect(Collectors.toMap(OrderProduct::getC_unique_id, r -> r));
    List<Product> productList = productService.getProductByIdSet(productIdOrderProductMap.keySet());

    Map<Long, Product> productIdProductMap = productList.stream().filter(Objects::nonNull)
        .collect(Collectors.toMap(Product::getId, r -> r));


    Boolean invalidProductIdPresent = false;
    List<Long> invalidProductIdList = new ArrayList<>();
    for (Map.Entry<Long, OrderProduct> entry : productIdOrderProductMap.entrySet()) {

      Long productId = entry.getKey();
      if (productIdProductMap.get(productId) == null) {
        invalidProductIdPresent = true;
        invalidProductIdList.add(productId);
        continue;
      }

    }

    if (invalidProductIdPresent) {
      throw new SaveoBusinessException(SaveoErrorCodeEnum.INVALID_PRODUCT_ID.getErrorCode(),
          SaveoErrorCodeEnum.INVALID_PRODUCT_ID.getErrorMessage()
              + invalidProductIdList.toString());
    }

    return orderHelperService.placeOrder(orderProductList);

  }

}
