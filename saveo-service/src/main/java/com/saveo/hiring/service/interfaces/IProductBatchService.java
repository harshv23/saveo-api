package com.saveo.hiring.service.interfaces;

import java.util.List;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.common.model.ProductBatchLite;
import com.saveo.hiring.service.model.ProductBatch;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IProductBatchService {

  Long saveProductBatch(ProductBatch productBatch) throws SaveoBusinessException;

  List<ProductBatchLite> getProductBatchLiteListByProductId(Long productId)
      throws SaveoBusinessException;

}
