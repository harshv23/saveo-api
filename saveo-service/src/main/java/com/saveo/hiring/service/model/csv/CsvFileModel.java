package com.saveo.hiring.service.model.csv;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class CsvFileModel implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -1074975402040513471L;

  // c_name
  private String medicineName;

  // c_manufacturer
  private String manufacturerName;

  // c_batch_no
  private String batchNo;

  // d_expiry_date
  private Date expiryDate;

  // n_balance_qty
  private Integer balanceQuantity;

  // c_packaging
  private String packaging;

  // c_unique_code
  private String productCode;

  // c_schemes
  private String schemes;

  // n_mrp
  private Double mrp;

  // hsn_code
  private String hsnCode;



  public String getMedicineName() {
    return medicineName;
  }



  public void setMedicineName(String medicineName) {
    this.medicineName = medicineName;
  }



  public String getManufacturerName() {
    return manufacturerName;
  }



  public void setManufacturerName(String manufacturerName) {
    this.manufacturerName = manufacturerName;
  }



  public String getBatchNo() {
    return batchNo;
  }



  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }



  public String getPackaging() {
    return packaging;
  }



  public void setPackaging(String packaging) {
    this.packaging = packaging;
  }



  public String getProductCode() {
    return productCode;
  }



  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }



  public String getSchemes() {
    return schemes;
  }



  public void setSchemes(String schemes) {
    this.schemes = schemes;
  }



  public Date getExpiryDate() {
    return expiryDate;
  }



  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }



  public Integer getBalanceQuantity() {
    return balanceQuantity;
  }



  public void setBalanceQuantity(Integer balanceQuantity) {
    this.balanceQuantity = balanceQuantity;
  }



  public Double getMrp() {
    return mrp;
  }



  public void setMrp(Double mrp) {
    this.mrp = mrp;
  }



  public String getHsnCode() {
    return hsnCode;
  }



  public void setHsnCode(String hsnCode) {
    this.hsnCode = hsnCode;
  }



  @Override
  public String toString() {
    return "CsvFileModel [medicineName=" + medicineName + ", manufacturerName=" + manufacturerName
        + ", batchNo=" + batchNo + ", expiryDate=" + expiryDate + ", balanceQuantity="
        + balanceQuantity + ", packaging=" + packaging + ", productCode=" + productCode
        + ", schemes=" + schemes + ", mrp=" + mrp + ", hsnCode=" + hsnCode + "]";
  }

}
