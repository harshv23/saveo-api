package com.saveo.hiring.service.interfaces;

import java.util.Map;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.model.Manufacturer;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IManufacturerService {

  Map<String, Manufacturer> getManufacturerMap() throws SaveoBusinessException;

  Manufacturer saveManufacturer(String manufacturerName) throws SaveoBusinessException;

  Map<Long, Manufacturer> getManufacturerIdMap() throws SaveoBusinessException;

  Manufacturer getManufacturerById(Long manufacturerId) throws SaveoBusinessException;

}
