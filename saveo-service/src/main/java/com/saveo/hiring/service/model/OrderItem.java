package com.saveo.hiring.service.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public class OrderItem implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 5545202209651887377L;

  private Long id;

  private Long orderId;
  private Long productId;
  private Integer quantity;
  private Boolean isActive;
  private Date createdOn;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  @Override
  public String toString() {
    return "OrderItem [id=" + id + ", orderId=" + orderId + ", productId=" + productId
        + ", quantity=" + quantity + ", isActive=" + isActive + ", createdOn=" + createdOn + "]";
  }

}
