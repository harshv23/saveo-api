package com.saveo.hiring.service.interfaces;

import java.util.Map;

import com.saveo.hiring.common.exception.SaveoBusinessException;
import com.saveo.hiring.service.model.PackagingType;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IPackagingTypeService {

  Map<String, PackagingType> getPackagingTypeMap() throws SaveoBusinessException;

  PackagingType savePackagingType(String packaging) throws SaveoBusinessException;

  Map<Long, PackagingType> getPackagingTypeIdMap() throws SaveoBusinessException;

  PackagingType getPackagingTypeById(Long packagingTypeId) throws SaveoBusinessException;

}
