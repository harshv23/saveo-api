package com.saveo.hiring.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.saveo.hiring.dao.model.PackagingTypeEntity;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IPackagingTypeRepository extends CrudRepository<PackagingTypeEntity, Long> {

}
