package com.saveo.hiring.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
@Entity
@Table(name = "orders")
public class OrderEntity implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 2616441434937634167L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String orderNo;
  private Date createdOn;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  @Override
  public String toString() {
    return "OrderEntity [id=" + id + ", orderNo=" + orderNo + ", createdOn=" + createdOn + "]";
  }


}
