package com.saveo.hiring.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
@Entity
@Table(name = "packaging_type")
public class PackagingTypeEntity implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -906706039805017278L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private Date createdOn;
  private String createdBy;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public String toString() {
    return "PackagingTypeEntity [id=" + id + ", name=" + name + ", createdOn=" + createdOn
        + ", createdBy=" + createdBy + "]";
  }

}
