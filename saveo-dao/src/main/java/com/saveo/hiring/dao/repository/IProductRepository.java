package com.saveo.hiring.dao.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.saveo.hiring.dao.model.ProductEntity;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IProductRepository extends CrudRepository<ProductEntity, Long> {

  ProductEntity getProductByCode(String code);

  List<ProductEntity> findByNameContaining(String name);

  @Query("Select pe from ProductEntity pe where  pe.id in (:idSet)")
  List<ProductEntity> getProductByIdSet(@Param("idSet") Set<Long> idSet);

}
