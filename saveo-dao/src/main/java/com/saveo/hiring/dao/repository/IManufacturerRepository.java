package com.saveo.hiring.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.saveo.hiring.dao.model.ManufacturerEntity;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IManufacturerRepository extends CrudRepository<ManufacturerEntity, Long> {

}
