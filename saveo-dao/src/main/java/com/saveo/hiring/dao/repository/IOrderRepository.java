package com.saveo.hiring.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.saveo.hiring.dao.model.OrderEntity;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IOrderRepository extends CrudRepository<OrderEntity, Long> {

}
