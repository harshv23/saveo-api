package com.saveo.hiring.dao.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.saveo.hiring.dao.model.ProductBatchEntity;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IProductBatchRepository extends CrudRepository<ProductBatchEntity, Long> {

  List<ProductBatchEntity> getProductBatchByProductIdAndIsActive(Long productId, Boolean isActive);

}
