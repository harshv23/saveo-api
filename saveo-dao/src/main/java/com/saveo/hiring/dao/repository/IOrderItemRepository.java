package com.saveo.hiring.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.saveo.hiring.dao.model.OrderItemEntity;

/**
 *
 * @author Harshwardhan Singh Muktawat
 *
 */
public interface IOrderItemRepository extends CrudRepository<OrderItemEntity, Long> {

}
